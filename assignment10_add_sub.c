#include<stdio.h>
int main()
{
    int m,n;
    printf("enter order of matrix m,n\n");
    scanf("%d%d",&m,&n);
    int a[m][n],b[m][n],sum[m][n],subtraction[m][n];
    printf("enter values of matrix a[%d][%d]\n",m,n);
    for(int i=0;i<m;i++)
        for(int j=0;j<n;j++)
            scanf("%d",&a[i][j]);
    printf("enter values of matrix b[%d][%d]\n",m,n);
    for(int i=0;i<m;i++)
        for(int j=0;j<n;j++)
            scanf("%d",&b[i][j]);

    printf("sum of matrix a and b :\n");
    for(int i=0;i<m;i++)
    {
        for(int j=0;j<n;j++)
        {
          sum[i][j]=a[i][j] + b[i][j];
          printf("%d\t",sum[i][j]);
        }

        printf("\n");
    }
    printf("subtraction of matrix a and b :\n");
    for(int i=0;i<m;i++)
    {
        for(int j=0;j<n;j++)
        {
            subtraction[i][j]=a[i][j] - b[i][j];
            printf("%d\t",subtraction[i][j]);
        }

        printf("\n");
    }
    return 0;
}
