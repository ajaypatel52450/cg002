#include<stdio.h>
void add(int *x,int *y,int *z)
{
*z=*x+*y;
}
void sub(int *x,int *y,int *z)
{
*z=*x-*y;
}
void mul(int *x,int *y,int *z)
{
*z=(*x)*(*y);
}
void div(int *x,int *y,float *z)
{
*z=(*x)/(*y);
}
void mod(int *x,int *y,int *z)
{
*z=(*x)%(*y);
}
int main()
{
int a,b,sum,su,mult,mo;
float di;
printf("Enter two numbers\n");
scanf("%d%d",&a,&b);
add(&a,&b,&sum);
printf("The sum of the two numbers is:%d\n",sum);
sub(&a,&b,&su);
printf("The difference of the two numbers is:%d\n",su);
mul(&a,&b,&mult);
printf("The multiplication of the two numbers is:%d\n",mult);
div(&a,&b,&di);
printf("The division of the two numbers is:%0.2f\n",di);
mod(&a,&b,&mo);
printf("The modulus of the two numbers is:%d",mo);
return 0;

}