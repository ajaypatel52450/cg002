#include<stdio.h>
void swap(int *x,int *y)
{
int temp;
temp=*x;
*x=*y;
*y=temp;
}
int main()
{
int a,b;
printf("Enter two numbers:\n");
scanf("%d%d",&a,&b);
printf("Numbers before swapping are:%d,%d\n",a,b);
swap(&a,&b);
printf("Numbers after swapping are:%d,%d",a,b);
return 0;

} 